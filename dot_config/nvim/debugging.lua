debugging = {
    debug_restart = false,
    debug = true,
    log_fh = nil,
    log = function(...)
        if debugging.debug then
            local objects = {}
            for i = 1, select('#', ...) do
                local v = select(i, ...)
                table.insert(objects, vim.inspect(v))
            end
            debugging.log_fh:write(table.concat(objects, '\n') .. '\n')
            debugging.log_fh:flush()
        end
    end,
    script_path = function()
        return debug.getinfo(2, "S").source:sub(2)
    end,
}
if debugging.debug then
    debugging.log_fh = io.open("/tmp/nvim." .. os.getenv("USER") .. ".debugging.log", debugging.debug_restart and 'w' or 'a')
end

debugging.log("Initialized")
