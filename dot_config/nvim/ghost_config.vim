augroup nvim_ghost_user_autocommands
  au User *github.com setfiletype markdown
  au User *gitlab.com setfiletype markdown
augroup END
