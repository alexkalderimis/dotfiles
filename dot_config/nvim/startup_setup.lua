startup = require"startup"

startup.create_mappings({
  ["<leader>fg"] = "<cmd>Git<CR>",
})
startup.setup()
