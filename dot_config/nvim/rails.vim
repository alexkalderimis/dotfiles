au FileType ruby nnoremap <silent> <leader>nn :OpenIRB<CR>
au FileType ruby nnoremap <silent> <leader>nr :OpenRailsC<CR>
au FileType ruby nnoremap <silent> <leader>nh :Tclose<CR>
au FileType ruby setl ts=2 sw=2 cc=100,120 tw=80
autocmd FileType ruby let b:dispatch = 'bundle exec rubocop %'
au FileType ruby command! AC :execute "e " . eval('rails#buffer().alternate()')

" Tags
au FileType ruby,haml command! MakeTags execute "!ctags -R --languages=ruby --exclude=.git --exclude=db/migrate --exclude=node_modules --exclude=qa --exclude=doc --exclude=log --exclude=vendor ."
