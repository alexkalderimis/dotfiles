call ale#Set('markdown_md_lint_executable', 'markdownlint')
call ale#Set('markdown_md_lint_use_global', get(g:, 'ale_use_global_executables', 0))

call ale#linter#Define('markdown', {
\   'name': 'md_lint',
\   'executable': {b -> ale#node#FindExecutable(b, 'markdown_md_lint', [
\       'node_modules/.bin/markdownlint',
\   ])},
\   'lint_file': 1,
\   'output_stream': 'both',
\   'command': '%e %s',
\   'callback': 'ale#handlers#markdownlint#Handle'
\})
