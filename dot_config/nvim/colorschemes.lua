-- local dark_theme = 'duskfox'
local dark_theme = 'monokai_pro'
local light_theme = 'dawnfox'
local default_theme = dark_theme

if vim.env.TERM_VARIANT == 'dark' then
  vim.cmd.colorscheme(dark_theme)
  vim.o.background = 'dark'
elseif vim.env.TERM_VARIANT == 'light' then
  vim.cmd.colorscheme(light_theme)
  vim.o.background = 'light'
else
  vim.cmd.colorscheme(default_theme)
end

vim.api.nvim_create_user_command(
  'MyEyes',
  function(opts)
    vim.o.background = 'dark'
    vim.cmd.colorscheme(dark_theme)
  end,
  { nargs = 0 }
)

vim.api.nvim_create_user_command(
  'TooDark',
  function(opts)
    vim.o.background = 'light'
    vim.cmd.colorscheme(light_theme)
  end,
  { nargs = 0 }
)
