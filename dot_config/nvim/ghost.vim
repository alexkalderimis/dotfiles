runtime ghost_config.vim

:call deoplete#custom#var('gitlab-users', 'project_path', 'gitlab-org/gitlab')
:call deoplete#custom#var('gitlab-labels', 'project_path', 'gitlab-org/gitlab')
:call deoplete#custom#var('gitlab-merge-requests', 'project_path', 'gitlab-org/gitlab')



if exists('g:neovide')
  let g:nvim_ghost_autostart = 1
else
  let g:nvim_ghost_autostart = 0
end
