local augroup = vim.api.nvim_create_augroup('yaml_commands', { clear = true })

vim.api.nvim_create_autocmd('FileType', {
  pattern = 'yaml',
  group = augroup,
  callback = function() 
    if vim.api.nvim_buf_line_count(0) > 100 then
      vim.opt.foldmethod = 'indent'
    end
  end
})
