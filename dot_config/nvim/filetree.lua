-- "" NERDTree configure
--
-- "Open NERDTree when nvim starts
-- autocmd StdinReadPre * let s:std_in=1
-- " autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
--
-- " NERDTree bindings
-- " mnemonic: nerd
-- map <C-n> :NERDTreeToggle<CR><C-w>p
-- " mnemonic: here
-- map <C-h> :NERDTreeFind<CR>
-- "Show hidden files in NERDTree
-- let NERDTreeShowHidden=1

-- disable netrw at the very start of your init.lua
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- set termguicolors to enable highlight groups
vim.opt.termguicolors = true

local function keymap_opts(desc)
  return { desc = "nvim-tree: " .. desc, noremap = true, silent = true, nowait = true }
end

local function my_on_attach(bufnr)
  local api = require "nvim-tree.api"

  local function opts(desc)
    local options = keymap_opts(desc)
    options.buffer = bufnr

    return options
  end

  -- default mappings
  api.config.mappings.default_on_attach(bufnr)

  -- custom mappings
  vim.keymap.set('n', '<C-t>', api.tree.change_root_to_parent,        opts('Up'))
  vim.keymap.set('n', '?',     api.tree.toggle_help,                  opts('Help'))
end

-- empty setup using defaults
require("nvim-tree").setup {
  on_attach = my_on_attach,
}

vim.keymap.set('n', '<C-n>', ':NvimTreeToggle<CR>', keymap_opts('Open/Close'))
vim.keymap.set('n', '<C-h>', ':NvimTreeFindFile<CR>', keymap_opts('Find file'))
vim.keymap.set('n', '<Space>', ':NvimTreeToggle<CR>', keymap_opts('Open/Close'))

