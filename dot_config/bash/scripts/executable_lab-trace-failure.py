#!/usr/bin/env python3

import argparse
import os
import re
import sys
import subprocess
from collections import namedtuple
import readline

parser = argparse.ArgumentParser(description = 'Read a failed CI job')
parser.add_argument('--name', metavar='NAME', type=str,
                    help='The name of the job (a unique prefix is enough)')
parser.add_argument('--index', metavar='INDEX', type=int,
                    help='The index of the job')
parser.add_argument('--all', action='store_true', help='include non-failed jobs')
parser.add_argument('--show-guff', action='store_true', help='Show all the test setup logging')
parser.add_argument('smart', metavar='INDEX', type=str, nargs='?',
                    help='A numeric suffix, or a string infix')

args = parser.parse_args()

cmd = ['lab', 'ci', 'status']
pattern = re.compile('^([^:]+):\\s+(.+)\\s+-\\s+(failed) id: (\\d+)$')
all_pattern = re.compile('^([^:]+):\\s+(.+)\\s+-\\s+(failed|running|created|success) id: (\\d+)$')

appendix_pattern = rb'section_end:\d+:step_script'
# Workaround for the fact that test traces are not available by section
setup_failed = rb'error: RPC failed; HTTP 500 curl 22 The requested URL returned error: 500'
start_pattern = rb'(intentionally failed|FATAL|failing tests|^You have \d+ pending migrations:|Finished in|documentation is outdated|Some static analyses failed:|ERROR: Job failed|failed validation:|Issues found!|ERROR|Error:|Errors:|Messages:|Offenses:|scripts/undercoverage|scripts/lint-doc.sh)'
ignore_pattern = rb'^(OK|SKIP)'

Job = namedtuple('Job', ['stage', 'name', 'job_id'])

def jobs(include_all = False):
    pat = all_pattern if include_all else pattern
    if not include_all:
        cmd.append('--failures')
    for line in subprocess.check_output(cmd).decode('utf-8').splitlines():
        match = re.search(pat, line)
        if match:
            yield Job._make(match.group(1,2,4))

def trace_job(job):
    cmd = ['lab', 'ci', 'trace', job.job_id]
    started = args.show_guff

    for line in subprocess.check_output(cmd).splitlines():
        if re.search(setup_failed, line):
            print('SETUP FAILED! rerun with: ci-restart {jid}'.format(jid = job.job_id))
            break

        if re.search(start_pattern, line):
            started = True

        if re.search(appendix_pattern, line):
            break
        else:
            if started and not re.search(ignore_pattern, line):
                sys.stdout.buffer.write(line)
                sys.stdout.buffer.write(b'\n')

    print("Finished tracing: {name} ({jid})".format(name = job.name.strip(), jid = job.job_id))
    os.sys.exit(0)

def run_index(jobs, idx):
    if idx in range(0, len(jobs)):
        trace_job(jobs[idx])
    else:
        print("No failed job at index {0}".format(idx))
        os.sys.exit(1)

def select_failure(jobs):
    if not jobs:
        print("No failed jobs! Go you :)")
        os.sys.exit(1)

    print("FAILED JOBS:")
    for i, job in enumerate(jobs):
        print(i, job)
    job_idx = input("Select a job (0 .. {0}): ".format(len(jobs) - 1))
    if job_idx:
        run_index(jobs, int(job_idx))
    else:
        os.sys.exit(1)

if args.smart and args.smart.isnumeric():
    matches = [job for job in jobs(args.all) if str(job.job_id).endswith(args.smart)]
    if not matches:
        print("No failed job matches {0}".format(args.smart))
        os.sys.exit(1)
    if len(matches) > 1:
        print("Multiple matches found for {0}:".format(args.smart))
        select_failure(matches)
    else:
        trace_job(matches[0])
elif args.name or args.smart:
    infix = (args.name or args.smart)
    matches = [job for job in jobs(args.all) if infix in job.name]
    if not matches:
        print("No job matches {0}".format(infix))
        os.sys.exit(1)
    if len(matches) > 1:
        print("Multiple matches found for {0}:".format(infix))
        select_failure(matches)
    else:
        trace_job(matches[0])
elif args.index is not None:
    if args.all:
        raise Exception('cannot use all with index')
    run_index(list(jobs()), args.index)
else:
    select_failure(list(jobs()))
