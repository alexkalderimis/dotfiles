#!/usr/bin/env python3

import sys
import os
import hashlib
import subprocess
import json
import yaml
import time
from io import StringIO
from html.parser import HTMLParser
from pathlib import Path
from itertools import tee, filterfalse

# Utils

def partition(f, iterable):
    t1, t2 = tee(iterable)
    return list(filter(f, t1)), list(filterfalse(f, t2))

class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.text = StringIO()
    def handle_data(self, d):
        self.text.write(d)
    def get_data(self):
        return self.text.getvalue()

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

# Constants

cache = Path.home() / '.cache/find-user'
team_dir = Path.home() / 'projects/gitlab/www-gitlab-com/data/team_members/person'
fresh = time.time() - (60 * 60) # Cache for 1hr

def main(user, use_cache = True, full = True, include_job_desc = False):
    m = hashlib.sha256()
    m.update(user.encode('utf8'))
    if full:
        m.update(b"+full")
    if include_job_desc:
        m.update(b"+desc")
    file = m.hexdigest()

    cache_file = cache / file

    if use_cache and cache_file.exists() and cache_file.stat().st_mtime >= fresh:
        sys.stdout.write(cache_file.read_text())
        sys.exit(0)
    else:
        query = """
        query($user: String, $path: ID!) {
          group(fullPath: $path) {
            members: groupMembers(search: $user) {
              nodes {
                user {
                  name
                  username
                  location
                  status { message }
                }
              }
            }
          }
        }
        """
        lab_args = ['lab', 'graphql', query, '-v', 'path:gitlab-org', '-v', 'user:' + user]

        results = subprocess.run(lab_args, capture_output = True)
        if results.returncode != 0:
            sys.stderr.write(results.stderr)
            sys.exit(1)
        data = json.loads(results.stdout)
        users = [node['user'] for node in data['group']['members']['nodes']]

        roles = {}
        if include_job_desc:
            for member_yaml in team_dir.glob('*/*.yml'):
                member = yaml.safe_load(member_yaml.read_text())
                if 'gitlab' in member and 'role' in member:
                    roles[member['gitlab']] = strip_tags(member['role'])

        with cache_file.open(mode = 'w') as f:
            for user in users:
                line = None
                if full:
                    status = user['status']['message'] if user['status'] else None
                    details = [part for part in [user['name'], user['location'], status] if part is not None and part != '']
                    if user['username'] in roles:
                        details.append(roles[user['username']])

                    line = "@{username} ({summary})".format(summary = ', '.join(details), **user)
                else:
                    line = user['username']
                f.write(line + "\n")
                print(line)

if __name__ == "__main__":
    flags, args = partition(lambda s: s.startswith('-'), sys.argv[1:])

    full = '--short' not in flags
    long = '--long' in flags
    use_cache = '--no-cache' not in flags
    help_wanted = '--help' in flags or '-h' in flags or 'help' in args
    bad_args = not args or len(args) > 1

    if help_wanted or bad_args:
        print("find-user [--short] [--no-cache] [--help] STR")
        sys.exit(1)

    if use_cache:
        cache.mkdir(exist_ok = True)

    main(args[0], use_cache = use_cache, full = full, include_job_desc = long)

