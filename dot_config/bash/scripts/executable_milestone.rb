#!/bin/env ruby

require 'json'
require 'pp'
require 'date'

QUERY = <<~GQL
  query {
    group(fullPath: "gitlab-org") {
      milestones(state: active) {
        nodes {
          title
          start: startDate
          end: dueDate
        }
      }
    }
  }
GQL

name = ARGV.shift || 'current'
oggi = Date.today

milestones = IO.popen(['lab', 'gql', QUERY]) do |io|
  JSON.parse(io.read, symbolize_names: true).dig(:group, :milestones, :nodes).map do |node|
    node.merge(node.slice(:start, :end).transform_values { |v| Date.parse(v) if v })
  end
end

case name
when 'current'
  ms = milestones.find { |m| (m[:start] <= oggi && oggi <= m[:end]) }
  puts ms[:title] if ms
when 'next'
  ms = milestones.select { |m| m[:start] }.select { |m| oggi < m[:start] }.min_by { |m| m[:start].iso8601 }
  puts ms[:title] if ms
else
  ms = begin
    date = Date.parse(name)
    milestones.find { |m| (m[:start] <= date && date <= m[:end]) }
  rescue Date::Error
    milestones.find { |m| m[:title] == name }
  end

  puts ("%{title}: %{start}..%{end}" % ms) if ms
end
