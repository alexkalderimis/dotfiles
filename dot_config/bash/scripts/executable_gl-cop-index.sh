#!/bin/bash

against=${1:-HEAD}

git diff-index --name-only --cached --diff-filter=ACMRT "$against" -- \
    | xargs ls -1 2>/dev/null \
    | grep ".rb$" \
    | grep -v "db/schema.rb" \
    | xargs --no-run-if-empty bundle exec rubocop
