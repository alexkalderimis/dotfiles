#!/bin/env python3

import sys
from contextlib import contextmanager
from pathlib import Path
import digitalocean

@contextmanager
def manager():
    with open(Path.home() / '.do-token') as f:
        token = f.read().strip()
        manager = digitalocean.Manager(token=token)
        yield manager

args = sys.argv[1:]

if args == ['ls']:
    with manager() as do:
        print("droplets:")
        for droplet in do.get_all_droplets():
            print(droplet)
else:
    print("Unknown arguments: " + str(args))
