puts '=' * 80

ApplicationSetting.current.update(prometheus_metrics_enabled: false)
puts "Prometheus metrics disabled"
