export PATH=$LOCAL_BIN:./bin:$PATH

if hash asdf 2>/dev/null; then
  if asdf plugin-list | grep -q golang; then
    export PATH=$PATH:$(asdf where golang)/packages/bin
  fi
fi

export PATH=$PATH:$HOME/.config/bash/scripts
