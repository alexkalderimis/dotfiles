alias gi="git"
alias g="git"
alias gits="git s"
alias gpf='git pf $(current-branch)'

alias new-branch='yarn && bundt-deps && rails db:migrate && rails db:migrate RAILS_ENV=test && ruby-tags'
alias watch-log='tail -f log/development.log  | grep -v rack-timeout'

alias ruby-tags="ctags -R --languages=ruby --exclude=.git --exclude=db/migrate --exclude=node_modules --exclude=qa --exclude=doc --exclude=log ."

alias failed="lab ci status --failures"
alias ci-job="python $HOME/.config/bash/scripts/lab-trace-failure.py"
alias ci-jobb="ci-job --show-guff"
alias ci-restart="$HOME/.config/bash/scripts/restart-failures"
alias ci-failed-rb="$HOME/.config/bash/scripts/list-rb-failures"
alias ci="lab mr jobs -c"
alias mr=ci
alias mr-url="lab mr show -j | jq -r .WebURL"
alias cf="ci -f"
alias cr="ci -r"
alias cmd_logging="LAB_LOGGO_SPEC='<root>=ERROR; cmd=DEBUG'"
alias query_debug="QUERY_RECORDER_DEBUG=1"

alias mr-list-branches="lab mr list --project --assignees --reviewers --number=100 --color --ci-status --diff-stats --show-target --show-branch --merge-status --notitle --author=@ --show-wip --age"
alias mr-list="lab mr list --project --reviewers --number=100 --color --ci-status --ci-time --diff-stats --merge-status --notitle --author=@ --show-wip --age --pipeline-summary --short-title"
alias review-list="lab mr list --number=100 --notitle --color --ci-status --ci-time --diff-stats --show-branch --show-author --reviewer=@ --mark-needing-attention --show-wip"

board() {
  mr-board
  review-board
}

mr-board() {
  echo '# MY MRS:'
  echo gitlab-org/gitlab
  mr-list gitlab-org/gitlab
  echo gitlab-org/security/gitlab
  mr-list gitlab-org/security/gitlab
}

alias mrs="lab mr list --reviewers --number=100 --color --ci-status-symbol --diff-stats --show-target --show-branch --notitle --author=@ --show-wip --age --milestone --truncate-ref 25 --truncate-branch 25"
alias mr-md="lab mr list --author=@ --markdown-layout"

review-board() {
  echo '# MY REVIEWS:'
  review-list -x
}

reviews() {
  echo '# ALL REVIEWS:'
  review-list
}

open-reviews() {
  for url in $(lab mr list --notitle --reviewer=@ --web-url --noref); do
    qtb "$url"
  done
}

alias jest='./node_modules/jest/bin/jest.js'

alias cdgl="cd $GCK_DIR/gitlab-rails"
alias cdlab="cd $LAB_PROJECT_DIR"

# What the branch?
alias wtb="git lo origin/master..."

alias nvim-ghost='nvim -s ~/.config/nvim/ghost.vim'

alias gtags="ctags -R --languages=go --exclude=.git --exclude=log ."

alias query-recorder-logs="cat log/test.log | perl -n -e 'print if /(QueryRecorder|^\s+-->)/'"

alias test-log-tail-errror="tail -f log/test.log | perl -ne 'print if /^\S*Error/.../^\s*$/'"

alias cop=rubo

alias push='git pf $(current-branch) && lab mr uncache && rm -f $(build-failure-log)'
alias heave='git pf $(current-branch) --no-verify && lab mr uncache'

changes-from-base() {
  base="$(merge-base)"

  git fetch origin "$base"
  git lo "origin/$base".. \
    | tac  \
    | $HOME/.config/bash/scripts/annotate-with-changes-in.pl
}

# On branches, this will return the branch name
# On non-branches, ""
alias current-branch="git symbolic-ref HEAD 2> /dev/null | sed -e 's/refs\\/heads\\///'"

default_branch () {
  git remote show origin | grep 'HEAD branch' | cut -d' ' -f5
}

alias devcloud-forget="ssh-keygen -f $HOME/.ssh/known_hosts -R gitlab.kalderim.is"

alias open-mr='qtb $(lab mr show -j | jq -r .WebURL)'

review-mr() {
  qtb $(lab mr show $1 -j | jq -r .WebURL)
}

alias goodcop="REVEAL_RUBOCOP_TODO=0"
alias badcop="REVEAL_RUBOCOP_TODO=1"

alias gg="goodcop git"

alias push-sec='git push security $(current-branch ) --no-verify --force-with-lease'
