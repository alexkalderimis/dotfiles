shopt -s expand_aliases

export GL_PAT="$(security find-generic-password -a gitlab -s api -w)"

alias issues='lab issue ls --mine --show-label=workflow'
alias deliverables='issues -l Deliverable'
alias eco-issues='issues -l "group::ecosystem"'

alias in-dev='issues --label="workflow::in dev"'
alias assignees="lab mr show -j | jq -Mr '.Assignees.Nodes[] | .Username'"

IN_REVIEW_LABEL="workflow::in review"

alias bot-notes="lab mr notes --no-wrap --author=gitlab-bot"
alias find-maintainer="ci-jobb --all danger | extract-maintainer"
alias create-mr="push && lab mr create -s -d -t Default"

assign-maintainer() {
  maintainer=$(find-maintainer)
  echo "Assigning $maintainer..."
  assign $maintainer maintainer 'Danger chooses you'
}

assign-reviewer() {
  category="${1:-backend}"
  reviewer=$(ci-danger | extract-reviewer "$category")
  echo "Assigning @$reviewer..."
  assign "$reviewer" first 'Danger chooses you'
}

label-mr-new-docs() {
  lab mr note \
    -m 'This MR introduces new documentation' \
    -m '/label ~"Technical Writing"' \
    -m '/label ~"docs::new"' \
    -m '/label ~"docs-only"'
}

label-mr-docs() {
  lab mr note \
    -m 'This is a documentation MR' \
    -m '/label ~"Technical Writing"' \
    -m '/label ~"docs-only"'
}

gl-rm-tmp() {
  rm -rf tmp/
  for file in $(git status -s | cut -d ' ' -f 3 | grep gitkeep); do
    git checkout $file
  done
}

gdk-be-services() {
  local action='start'
  if [ -n "$1" ]; then
    action=$1
  fi
  for service in postgresql redis gitaly; do
    gdk $action $service
  done
}

gdk-test-services() {
  local action='start'
  if [ -n "$1" ]; then
    action=$1
  fi
  gl-be-services $action
  for service in elasticsearch webpack; do
    gdk $action $service
  done
}

gdk-run-app() {
  local action='start'
  if [ -n "$1" ]; then
    action=$1
  fi
  gl-test-services $action
  for service in minio gitlab-pages gitlab-workhorse rails-web rails-background-jobs; do
    gdk $action $service
  done
}

alias ci-danger="ci-jobb --all danger"
alias ci-danger-msgs="ci-jobb --all danger | perl -ne '\$results = /(Errors|Warnings|Messages|- \\[ \\])/ .. /^\\s*$/; print if \$results;'"

alias gql-auto-docs="be rake gitlab:graphql:schema:dump gitlab:graphql:compile_docs"

search-labels() {
  lab gql "query { group(fullPath: \"${2:-gitlab-org}\") { labels(searchTerm: \"${1}\") { nodes { title } } } }" \
    | jq '.group.labels.nodes | .[] | .title' \
    | grep "${1}"
}

alias suggest-reviewer='find-reviewer --token $GL_PAT'

gcurl-json() {
  graphqurl \
    "$@" \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json'
}

gql-repl() {
  gcurl-json \
    'https://gitlab.com/api/graphql' \
    -H "Authorization: Bearer $GL_PAT" \
    "$@"
}

gql-repl-anon() {
  gcurl-json \
    'https://gitlab.com/api/graphql' \
    "$@"
}

gql-repl-dev() {
  gcurl-json \
    "http://kaki.local:3000/api/graphql" \
    -H "Authorization: Bearer $(secret-tool lookup api-token gitlab-dev)" \
    "$@"
}

alias rbimb='git rebase -i $(merge-base)'

alias sec-mrs='mr-list -T=gitlab-org/security/gitlab'

export GCK="$HOME/projects/gitlab/gck"

if [[ -f "$GCK/.gck" ]]; then
    source "$GCK/.gck"
fi

export GL_BUNDLE_EXEC="$HOME/.config/bash/scripts/gck-be"
export GL_RUN_HOOK_COMMAND="$HOME/.config/bash/scripts/gck-do"
