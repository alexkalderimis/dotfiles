if [ -e /home/linuxbrew/.linuxbrew/bin/brew ]; then
  eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
  eval "$(/home/linuxbrew/.linuxbrew/bin/starship init bash)"
fi

if [ -e /opt/homebrew/bin/brew ]; then
  eval "$(/opt/homebrew/bin/brew shellenv)"
  eval "$(/opt/homebrew/bin/starship init bash)"
fi

if hash brew 2>/dev/null; then
  RUBY_CONFIGURE_OPTS="$RUBY_CONFIGURE_OPTS --with-zlib-dir=$(brew --prefix zlib)"
  RUBY_CONFIGURE_OPTS="$RUBY_CONFIGURE_OPTS --with-readline-dir=$(brew --prefix readline)"
  RUBY_CONFIGURE_OPTS="$RUBY_CONFIGURE_OPTS --with-libyaml-dir=$(brew --prefix libyaml)"
  RUBY_CONFIGURE_OPTS="$RUBY_CONFIGURE_OPTS --with-openssl-dir=$(brew --prefix openssl@3)"

  export RUBY_CONFIGURE_OPTS
fi
