# Set up the editor for the terminal
if has nvim; then
  export EDITOR=nvim
elif has vim; then
  export EDITOR=vim
elif has vi; then
  echo "warning: falling back to VI"
  export EDITOR=vi
fi
