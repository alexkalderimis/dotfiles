FG="38;5;"

color_256() {
  echo -n "\033[${FG}$1m"
}

bold_color_256() {
  echo -n "\033[1;${FG}$1m"
}

export PROMPT_USER_COLOR="$(bold_color_256 4)"
export PROMPT_PREPOSITION_COLOR="$(bold_color_256 6)"
export PROMPT_DEVICE_COLOR="$(bold_color_256 10)"
export PROMPT_DIR_COLOR="$(bold_color_256 11)"
export PROMPT_GIT_STATUS_COLOR="$(color_256 12)"
